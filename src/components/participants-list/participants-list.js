import ParticipantsListItem from "../participants-list-item/participants-list-item";
import './participants-list.css'

const Participantslist = ({ data }) => {
	const elements = data.map(item => {
		const { id, ...itemProps } = item;
		return (
			<ParticipantsListItem key={id} {...itemProps}></ParticipantsListItem>
		);
	});
	return (
		<ul className="app-list list-group">
			{elements}
		</ul>
	);
}

export default Participantslist;