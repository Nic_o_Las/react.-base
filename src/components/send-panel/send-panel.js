import './send-panel.css';

const SendPanel = () => {
	return (
		<form>
			<textarea className="form-control send-panel" rows="1" placeholder="Your message">
			</textarea>
			<button type="submit" className="btn btn-primary">Send</button>
		</form>
	);
}

export default SendPanel;