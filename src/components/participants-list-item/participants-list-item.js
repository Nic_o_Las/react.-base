import './participants-list-item.css'




const ParticipantsListItem = ({ userId, avatar, user, text, createdAt, editedAt }) => {
	return (
		<li className="list-group-item d-flex">
			<span className="list-group-item-label">
				<img src={avatar} alt="avatar" />
				{user}</span>
			<p className="list-group-item-text">
				{text}<br />{createdAt}
			</p>
			<div className="list-group-item-icon d-flex justify-content-center ms-auto">
				<button className="btn btn-edit">{editedAt}
					<i className="fa fa-edit"></i>
				</button>
				<button className="btn btn-trash">
					<i className="fa fa-trash"></i>
				</button>
				<button className="btn btn-like">
					<i className="fa fa-thumbs-up"></i>
				</button>
			</div>
		</li>
	);
}

export default ParticipantsListItem;